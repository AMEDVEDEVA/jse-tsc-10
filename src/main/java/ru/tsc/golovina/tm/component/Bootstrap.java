package ru.tsc.golovina.tm.component;

import ru.tsc.golovina.tm.api.controller.ICommandController;
import ru.tsc.golovina.tm.api.controller.IProjectController;
import ru.tsc.golovina.tm.api.controller.ITaskController;
import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.ICommandService;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.constant.ArgumentConst;
import ru.tsc.golovina.tm.constant.TerminalConst;
import ru.tsc.golovina.tm.controller.CommandController;
import ru.tsc.golovina.tm.controller.ProjectController;
import ru.tsc.golovina.tm.controller.TaskController;
import ru.tsc.golovina.tm.repository.CommandRepository;
import ru.tsc.golovina.tm.repository.ProjectRepository;
import ru.tsc.golovina.tm.repository.TaskRepository;
import ru.tsc.golovina.tm.service.CommandService;
import ru.tsc.golovina.tm.service.ProjectService;
import ru.tsc.golovina.tm.service.TaskService;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(final String[] args) {
        displayWelcome();
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exitSuccess();
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showArgumentError();
                commandController.exitError();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exitSuccess();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            default:
                commandController.showCommandError();
        }
    }

}
